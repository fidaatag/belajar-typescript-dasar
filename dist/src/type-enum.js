// * By default enum will be number after compile
// * be careful, if position index change, number will be change, so better do initialize first
// * you can initialize to be string or number or etc
export var CustomerType;
(function (CustomerType) {
    CustomerType["REGULAR"] = "REGULAR";
    CustomerType["GOLD"] = "GOLD";
    CustomerType["PLATINUM"] = "PLATINUM";
})(CustomerType || (CustomerType = {}));
