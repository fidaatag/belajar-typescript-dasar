"use strict";
describe('Any type data', function () {
    it('should support any in typescript', function () {
        const person = {
            id: 1,
            name: 'fidaa',
            age: 10
        };
        person.age = 50;
        person.name = 'lala';
        console.info(person);
    });
});
