"use strict";
describe('Array', function () {
    it('should same with javascript', function () {
        const name = ['fidaa', 'ana', 'lala'];
        const values = [1, 2, 3];
    });
    it('should support readonly arrat', function () {
        const hobbies = ['membaca', 'menulis'];
        console.info(hobbies[0]);
        console.info(hobbies[1]);
        // hobbies[0] = 'main game'             // * Index signature in type 'readonly string[]' only permits reading.
    });
    it('should support tupple', function () {
        const person = ['fidaa', 'lala', 123];
        // * type data tupple : an array data type that has a specified type for each index
        console.info(person);
        console.info(person[0]);
        console.info(person[1]);
        console.info(person[2]);
        // person[0] = 'budi'                   // * Cannot assign to '0' because it is a read-only property.
    });
});
