describe('interface', function () {
    it('should support in typescript', function () {
        const seller = {
            id: 1,
            name: 'Toko Roti',
            nib: '123412',
            npwp: '432423',
        };
        // seller.nib = '123123213'   // * Cannot assign to 'nib' because it is a read-only property.
        console.info(seller);
    });
    it('should support function interface', function () {
        const add = (value1, value2) => {
            return value1 + value2;
        };
        expect(add(1, 2)).toBe(3);
    });
    it('should support indexable interface', function () {
        const name = ['Jakarta', 'Bandung', 'Semarang'];
        console.info(name);
    });
    it('should support indexable interface for non number index', function () {
        const dictionary = {
            'name': 'Fidaa',
            'address': 'Indonesia'
        };
        expect(dictionary["name"]).toBe('Fidaa');
        expect(dictionary["address"]).toBe('Indonesia');
    });
});
export {};
