"use strict";
describe('Optional parameter', function () {
    it('should support null and undefined', function () {
        function sayHello(name) {
            if (name) {
                console.info(`Hello ${name}`);
            }
            else {
                console.info('Hello');
            }
        }
        sayHello('Fidaa');
        // const name: string | undefined                   // * 'const' declarations must be initialized.
        const name = undefined;
        sayHello(name);
        sayHello(null);
        // * null and undefined is different
    });
});
