import { sayHello } from "../src/say-hello";
describe('sayHello', function () {
    it('should say hello fidaa', function () {
        expect(sayHello("fidaa")).toBe("Hello fidaa");
    });
});
