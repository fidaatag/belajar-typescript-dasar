describe('Type Alias', function () {
    it('should support type alias in typescript', function () {
        const category = {
            id: '1',
            name: 'handphone'
        };
        const product = {
            id: 1,
            name: 'Samsung S20',
            price: 200000000,
            category: category
        };
        console.info(category);
        console.info(product);
        // product.description = 'Lalala yeyeyey'              // * Property 'description' does not exist on type 'Product'.
    });
});
export {};
