import { CustomerType } from "../src/type-enum";
describe('Enum', function () {
    it('should support enum type in typescript', function () {
        const customer = {
            id: 1,
            name: 'fidaa',
            type: CustomerType.GOLD
        };
        console.info(customer);
    });
});
