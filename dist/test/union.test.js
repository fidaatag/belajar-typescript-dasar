"use strict";
describe('union type data', function () {
    it('should support union in typescript', function () {
        let sample = 'fidaa';
        console.info(sample);
        sample = 100;
        console.info(sample);
        sample = 'lala';
        console.info(sample);
        // sample = [1, 2, 4]                 // * Type 'number[]' is not assignable to type 'string | number | boolean'
        // * union like any, but can change as specified
        // * ex: you can change value 'let sampel' by 3 option type data : number or string or boolean
        // * better, do checking type data using typeof before execute 
    });
    it('should support typeof operator', function () {
        function process(value) {
            if (typeof value === 'string') {
                return value.toUpperCase();
            }
            else if (typeof value === 'number') {
                return value + 2;
            }
            else {
                return !value;
            }
        }
        expect(process('Fidaa')).toBe('FIDAA');
        expect(process(10)).toBe(12);
        expect(process(true)).toBe(false);
    });
});
