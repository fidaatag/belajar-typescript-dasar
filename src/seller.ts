export interface Seller {
    id: number;
    name: string;
    address?: string;
    readonly nib: string;           // * after data filled become readonly
    readonly npwp: string;
}