
// * Alias type for Union type
export type ID = string | number

// * Alias type
export type Category = {
    id: ID;
    name: string;
}

export type Product = {
    id: ID;
    name: string;
    price: number;
    category: Category;
}