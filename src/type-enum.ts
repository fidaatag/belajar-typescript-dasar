// * By default enum will be number after compile
// * be careful, if position index change, number will be change, so better do initialize first
// * you can initialize to be string or number or etc
export enum CustomerType {
    REGULAR = 'REGULAR',
    GOLD = 'GOLD',
    PLATINUM = 'PLATINUM'
}

export type Customer = {
    id: number;
    name: string;
    type: CustomerType
}