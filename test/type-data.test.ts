describe('Data type', function () {
  it('should must declare', function () {

    let name: string = "Fidaa"
    let balance: number = 100000
    let isVip: boolean = true

    console.info(name)
    console.info(balance)
    console.info(isVip)

    // name = 1          // error if run npm tsc, but when npm test will run well
    // balance = "100"   // error
    // isVip = 1         // error
    
  })
})